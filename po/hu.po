# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Balázs Meskó <meskobalazs@mailbox.org>, 2017-2018
# Benedek Imre <nucleo@indamail.hu>, 2013-2014
# Egmont Koblinger <egmont@uhulinux.hu>, 2003
# Gabor Kelemen <kelemeng at gnome dot hu>, 2009-2010,2012
# Gábor P., 2019-2020
# gyeben <gyonkibendeguz@gmail.com>, 2016
# gyeben <gyonkibendeguz@gmail.com>, 2013
# kelemeng <kelemeng@ubuntu.com>, 2013
# László Stygár, 2017
# Somogyi Peter <jerry@gnu>, 2003
msgid ""
msgstr ""
"Project-Id-Version: Xfdesktop\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-17 00:53+0100\n"
"PO-Revision-Date: 2021-11-16 23:53+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>\n"
"Language-Team: Hungarian (http://www.transifex.com/xfce/xfdesktop/language/hu/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hu\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../settings/main.c:419 ../src/xfdesktop-special-file-icon.c:290
#: ../src/xfdesktop-special-file-icon.c:476
msgid "Home"
msgstr "Saját mappa"

#: ../settings/main.c:421 ../src/xfdesktop-special-file-icon.c:292
#: ../src/xfdesktop-special-file-icon.c:474
msgid "File System"
msgstr "Fájlrendszer"

#: ../settings/main.c:423 ../src/xfdesktop-special-file-icon.c:294
msgid "Trash"
msgstr "Kuka"

#: ../settings/main.c:425
msgid "Removable Devices"
msgstr "Cserélhető eszközök"

#: ../settings/main.c:427
msgid "Network Shares"
msgstr "Hálózati megosztások"

#: ../settings/main.c:429
msgid "Disks and Drives"
msgstr "Lemezek és meghajtók"

#: ../settings/main.c:431
msgid "Other Devices"
msgstr "Egyéb eszközök"

#. Display the file name, file type, and file size in the tooltip.
#: ../settings/main.c:541
#, c-format
msgid ""
"<b>%s</b>\n"
"Type: %s\n"
"Size: %s"
msgstr "<b>%s</b>\nTípus: %s\nMéret: %s"

#: ../settings/main.c:728
#, c-format
msgid "Wallpaper for Monitor %d (%s)"
msgstr "Háttérkép a %d monitorhoz (%s)"

#: ../settings/main.c:731
#, c-format
msgid "Wallpaper for Monitor %d"
msgstr "Háttérkép a %d monitorhoz"

#: ../settings/main.c:737
msgid "Move this dialog to the display you want to edit the settings for."
msgstr "Párbeszédablak mozgatása arra a képernyőre, amelyet be szeretne állítani."

#: ../settings/main.c:744
#, c-format
msgid "Wallpaper for %s on Monitor %d (%s)"
msgstr "Háttérkép ehhez: %s, a(z) %d. monitoron (%s)"

#: ../settings/main.c:748
#, c-format
msgid "Wallpaper for %s on Monitor %d"
msgstr "Háttérkép ehhez: %s, a(z) %d. monitoron"

#: ../settings/main.c:755
msgid ""
"Move this dialog to the display and workspace you want to edit the settings "
"for."
msgstr "Párbeszédablak mozgatása arra a képernyőre és munkaterületre, amelyet be szeretne állítani."

#. Single monitor and single workspace
#: ../settings/main.c:763
#, c-format
msgid "Wallpaper for my desktop"
msgstr "Háttérkép az asztalhoz"

#. Single monitor and per workspace wallpaper
#: ../settings/main.c:769
#, c-format
msgid "Wallpaper for %s"
msgstr "Háttérkép ehhez: %s"

#: ../settings/main.c:774
msgid "Move this dialog to the workspace you want to edit the settings for."
msgstr "Húzza ezt a párbeszédablakot arra a munkaterületre, amelyiket be szeretné állítani."

#: ../settings/main.c:1190
msgid "Image selection is unavailable while the image style is set to None."
msgstr "A képkiválasztás nem érhető el amíg a képstílus beállítása „Nincs”."

#: ../settings/main.c:1550
msgid "Spanning screens"
msgstr "Összekapcsolt képernyők"

#. TRANSLATORS: Please split the message in half with '\n' so the dialog will
#. not be too wide.
#: ../settings/main.c:1718
msgid ""
"Would you like to arrange all existing\n"
"icons according to the selected orientation?"
msgstr "Szeretné elrendezni az összes létező\nikont a kiválasztott tájolásnak megfelelően?"

#: ../settings/main.c:1724
msgid "Arrange icons"
msgstr "Ikonok rendezése"

#. printf is to be translator-friendly
#: ../settings/main.c:1732 ../src/xfdesktop-file-icon-manager.c:817
#: ../src/xfdesktop-file-icon-manager.c:1378
#, c-format
msgid "Unable to launch \"%s\":"
msgstr "„%s” nem indítható:"

#: ../settings/main.c:1733 ../src/xfdesktop-file-icon-manager.c:819
#: ../src/xfdesktop-file-icon-manager.c:1127
#: ../src/xfdesktop-file-icon-manager.c:1379 ../src/xfdesktop-file-utils.c:682
#: ../src/xfdesktop-file-utils.c:1216 ../src/xfdesktop-file-utils.c:1294
#: ../src/xfdesktop-file-utils.c:1318 ../src/xfdesktop-file-utils.c:1380
msgid "Launch Error"
msgstr "Indítási hiba"

#: ../settings/main.c:1735 ../settings/xfdesktop-settings-ui.glade.h:20
#: ../src/xfdesktop-file-icon-manager.c:582
#: ../src/xfdesktop-file-icon-manager.c:601
#: ../src/xfdesktop-file-icon-manager.c:686
#: ../src/xfdesktop-file-icon-manager.c:821
#: ../src/xfdesktop-file-icon-manager.c:1131
#: ../src/xfdesktop-file-icon-manager.c:1381
#: ../src/xfdesktop-file-icon-manager.c:2978 ../src/xfdesktop-file-utils.c:685
#: ../src/xfdesktop-file-utils.c:705 ../src/xfdesktop-file-utils.c:760
#: ../src/xfdesktop-file-utils.c:824 ../src/xfdesktop-file-utils.c:885
#: ../src/xfdesktop-file-utils.c:946 ../src/xfdesktop-file-utils.c:994
#: ../src/xfdesktop-file-utils.c:1049 ../src/xfdesktop-file-utils.c:1107
#: ../src/xfdesktop-file-utils.c:1159 ../src/xfdesktop-file-utils.c:1220
#: ../src/xfdesktop-file-utils.c:1296 ../src/xfdesktop-file-utils.c:1322
#: ../src/xfdesktop-file-utils.c:1384 ../src/xfdesktop-file-utils.c:1443
#: ../src/xfdesktop-file-utils.c:1459 ../src/xfdesktop-file-utils.c:1521
#: ../src/xfdesktop-file-utils.c:1539 ../src/xfdesktop-volume-icon.c:521
#: ../src/xfdesktop-volume-icon.c:567 ../src/xfdesktop-volume-icon.c:603
msgid "_Close"
msgstr "_Bezár"

#: ../settings/main.c:1890
msgid "Image files"
msgstr "Képfájlok"

#. Change the title of the file chooser dialog
#: ../settings/main.c:1898
msgid "Select a Directory"
msgstr "Válasszon könyvtárat"

#: ../settings/main.c:2107
msgid "Settings manager socket"
msgstr "Beállításkezelő foglalat"

#: ../settings/main.c:2107
msgid "SOCKET ID"
msgstr "FOGLALATAZONOSÍTÓ"

#: ../settings/main.c:2108
msgid "Version information"
msgstr "Verzióinformációk"

#: ../settings/main.c:2109 ../src/xfdesktop-application.c:842
msgid "Enable debug messages"
msgstr "Hibakeresési üzenetek engedélyezése"

#: ../settings/main.c:2133
#, c-format
msgid "Type '%s --help' for usage."
msgstr "Segítségért adja ki az „%s --help” parancsot."

#: ../settings/main.c:2145
msgid "The Xfce development team. All rights reserved."
msgstr "Az Xfce fejlesztőcsapata. Minden jog fenntartva."

#: ../settings/main.c:2146
#, c-format
msgid "Please report bugs to <%s>."
msgstr "A hibákat ide jelentse: <%s>."

#: ../settings/main.c:2153
msgid "Desktop Settings"
msgstr "Asztal beállításai"

#: ../settings/main.c:2155
msgid "Unable to contact settings server"
msgstr "Nem érhető el a beállításkiszolgáló"

#: ../settings/main.c:2157
msgid "Quit"
msgstr "Kilépés"

#: ../settings/xfce-backdrop-settings.desktop.in.in.h:1
#: ../settings/xfdesktop-settings-ui.glade.h:18 ../src/xfce-desktop.c:1170
msgid "Desktop"
msgstr "Asztal"

#: ../settings/xfce-backdrop-settings.desktop.in.in.h:2
msgid "Set desktop background and menu and icon behavior"
msgstr "Az asztalháttér, valamint a menü- és ikonviselkedés beállítása"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:1
msgid "Solid color"
msgstr "Homogén szín"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:2
msgid "Horizontal gradient"
msgstr "Vízszintes színátmenet"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:3
msgid "Vertical gradient"
msgstr "Függőleges színátmenet"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:4
msgid "Transparent"
msgstr "Átlátszó"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:5
msgid "Choose the folder to select wallpapers from."
msgstr "Háttérképeket tartalmazó mappa kiválasztása."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:6
msgid "St_yle:"
msgstr "Stíl_us:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:7
msgid "Specify how the image will be resized to fit the screen."
msgstr "A kép kijelzőn való átméretezésének és illesztésének megadása."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:8
#: ../settings/xfdesktop-settings-ui.glade.h:4
msgid "None"
msgstr "Nincs"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:9
msgid "Centered"
msgstr "Középre"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:10
msgid "Tiled"
msgstr "Mozaik"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:11
msgid "Stretched"
msgstr "Nyújtott"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:12
msgid "Scaled"
msgstr "Átméretezett"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:13
msgid "Zoomed"
msgstr "Nagyított"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:14
msgid "Spanning Screens"
msgstr "Összekapcsolt képernyők"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:15
msgid "Specify the style of the color drawn behind the backdrop image."
msgstr "A háttérkép mögötti szín megadása"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:16
msgid "Specifies the solid color, or the \"left\" or \"top\" color of the gradient."
msgstr "Egy szín megadása, vagy az átmenet „bal oldali” vagy „felső” színe."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:17
msgid "Select First Color"
msgstr "Válassza ki az első színt"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:18
msgid "Specifies the \"right\" or \"bottom\" color of the gradient."
msgstr "Az átmenet „jobb oldali” vagy „alsó” színe"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:19
msgid "Select Second Color"
msgstr "Válassza ki a második színt"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:20
msgid "Apply to all _workspaces"
msgstr "Alkalmazás az összes _munkaterületre"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:21
msgid "_Folder:"
msgstr "_Mappa:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:22
msgid "C_olor:"
msgstr "Sz_ín:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:23
msgid "Change the _background "
msgstr "_Háttér módosítása"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:24
msgid ""
"Automatically select a different background from the current directory."
msgstr "Automatikusan választ másik hátteret az aktuális könyvtárból."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:25
msgid "Specify how often the background will change."
msgstr "Háttér változtatásának gyakorisága"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:26
msgid "in seconds:"
msgstr "másodpercben:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:27
msgid "in minutes:"
msgstr "percben:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:28
msgid "in hours:"
msgstr "órában:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:29
msgid "at start up"
msgstr "induláskor"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:30
msgid "every hour"
msgstr "minden órában"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:31
msgid "every day"
msgstr "minden nap"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:32
msgid "chronologically"
msgstr "időrendben"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:33
msgid "Amount of time before a different background is selected."
msgstr "Másik háttérkép választásának időköze."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:34
msgid "_Random Order"
msgstr "Véletlen so_rrend"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:35
msgid ""
"Randomly selects another image from the same directory when the wallpaper is"
" to cycle."
msgstr "Véletlenszerűen választ egy másik képet ugyanabból a könyvtárból, ha a háttérkép váltakozóra van állítva."

#: ../settings/xfdesktop-settings-ui.glade.h:1
msgid "Left"
msgstr "Balra"

#: ../settings/xfdesktop-settings-ui.glade.h:2
msgid "Middle"
msgstr "Középre"

#: ../settings/xfdesktop-settings-ui.glade.h:3
msgid "Right"
msgstr "Jobbra"

#: ../settings/xfdesktop-settings-ui.glade.h:5
msgid "Shift"
msgstr "Shift"

#: ../settings/xfdesktop-settings-ui.glade.h:6
msgid "Alt"
msgstr "Alt"

#: ../settings/xfdesktop-settings-ui.glade.h:7
msgid "Control"
msgstr "Control"

#: ../settings/xfdesktop-settings-ui.glade.h:8
msgid "Minimized application icons"
msgstr "Minimalizált alkalmazásikonok"

#: ../settings/xfdesktop-settings-ui.glade.h:9
msgid "File/launcher icons"
msgstr "Fájl/indítóikonok"

#: ../settings/xfdesktop-settings-ui.glade.h:10
msgid "Top Left Vertical"
msgstr "Bal felső függőleges"

#: ../settings/xfdesktop-settings-ui.glade.h:11
msgid "Top Left Horizontal"
msgstr "Bal felső vízszintes"

#: ../settings/xfdesktop-settings-ui.glade.h:12
msgid "Top Right Vertical"
msgstr "Jobb felső függőleges"

#: ../settings/xfdesktop-settings-ui.glade.h:13
msgid "Top Right Horizontal"
msgstr "Jobb felső vízszintes"

#: ../settings/xfdesktop-settings-ui.glade.h:14
msgid "Bottom Left Vertical"
msgstr "Bal alsó függőleges"

#: ../settings/xfdesktop-settings-ui.glade.h:15
msgid "Bottom Left Horizontal"
msgstr "Bal alsó vízszintes"

#: ../settings/xfdesktop-settings-ui.glade.h:16
msgid "Bottom Right Vertical"
msgstr "Jobb alsó függőleges"

#: ../settings/xfdesktop-settings-ui.glade.h:17
msgid "Bottom Right Horizontal"
msgstr "Jobb alsó vízszintes"

#: ../settings/xfdesktop-settings-ui.glade.h:19
msgid "_Help"
msgstr "_Súgó"

#: ../settings/xfdesktop-settings-ui.glade.h:21
msgid "_Background"
msgstr "Hátté_r"

#: ../settings/xfdesktop-settings-ui.glade.h:22
msgid "Enable \"Delete\" option in file context menu"
msgstr ""

#: ../settings/xfdesktop-settings-ui.glade.h:23
msgid "Include applications menu on _desktop right click"
msgstr "Alkalmazások m_enü megjelenítése az asztalon jobb kattintásra"

#: ../settings/xfdesktop-settings-ui.glade.h:24
msgid "_Button:"
msgstr "G_omb:"

#: ../settings/xfdesktop-settings-ui.glade.h:25
msgid "Mo_difier:"
msgstr "Mó_dosító:"

#: ../settings/xfdesktop-settings-ui.glade.h:26
msgid "Show _application icons in menu"
msgstr "Alkalma_zásikonok megjelenítése a menüben"

#: ../settings/xfdesktop-settings-ui.glade.h:27
msgid "_Edit desktop menu"
msgstr "As_ztali menü szerkesztése"

#: ../settings/xfdesktop-settings-ui.glade.h:28
msgid "<b>Desktop Menu</b>"
msgstr "<b>Asztali menü</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:29
msgid "Show _window list menu on desktop middle click"
msgstr "Ablaklista me_nü megjelenítése az asztalon középső kattintásra"

#: ../settings/xfdesktop-settings-ui.glade.h:30
msgid "B_utton:"
msgstr "_Gomb:"

#: ../settings/xfdesktop-settings-ui.glade.h:31
msgid "Modi_fier:"
msgstr "Mód_osító:"

#: ../settings/xfdesktop-settings-ui.glade.h:32
msgid "Sh_ow application icons in menu"
msgstr "_Alkalmazásikonok megjelenítése a menüben"

#: ../settings/xfdesktop-settings-ui.glade.h:33
msgid "Show workspace _names in list"
msgstr "M_unkaterület-nevek megjelenítése a listában"

#: ../settings/xfdesktop-settings-ui.glade.h:34
msgid "Use _submenus for the windows in each workspace"
msgstr "Almenük _használata az ablakokhoz minden munkaterületen"

#: ../settings/xfdesktop-settings-ui.glade.h:35
msgid "Show s_ticky windows only in active workspace"
msgstr "A min_dig látható ablakok megjelenítése csak az aktív munkaterületen"

#: ../settings/xfdesktop-settings-ui.glade.h:36
msgid "Show a_dd and remove workspace options in list"
msgstr "Munkaterület hozzáa_dása és eltávolítása lehetőség megjelenítése a listában"

#: ../settings/xfdesktop-settings-ui.glade.h:37
msgid "<b>Window List Menu</b>"
msgstr "<b>Ablaklista menü</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:38
msgid "_Menus"
msgstr "_Menük"

#: ../settings/xfdesktop-settings-ui.glade.h:39
msgid "Icon _type:"
msgstr "Ikon_típus:"

#: ../settings/xfdesktop-settings-ui.glade.h:40
msgid "Icon _size:"
msgstr "Ikon_méret:"

#: ../settings/xfdesktop-settings-ui.glade.h:41
msgid "48"
msgstr "48"

#: ../settings/xfdesktop-settings-ui.glade.h:42
msgid "Icons _orientation:"
msgstr "Ikonok _tájolása:"

#: ../settings/xfdesktop-settings-ui.glade.h:43
msgid "Show icons on primary display"
msgstr "Ikonok megjelenítése az elsődleges kijelzőn"

#: ../settings/xfdesktop-settings-ui.glade.h:44
msgid "Use custom _font size:"
msgstr "_Egyéni betűméret használata:"

#: ../settings/xfdesktop-settings-ui.glade.h:45
msgid "12"
msgstr "12"

#: ../settings/xfdesktop-settings-ui.glade.h:46
msgid "Show icon tooltips. Size:"
msgstr "Súgóbuborékok megjelenítése. Méret:"

#: ../settings/xfdesktop-settings-ui.glade.h:47
msgid "Size of the tooltip preview image."
msgstr "A súgóbuborék előnézeti képének mérete."

#: ../settings/xfdesktop-settings-ui.glade.h:48
msgid "64"
msgstr ""

#: ../settings/xfdesktop-settings-ui.glade.h:49
msgid "Show t_humbnails"
msgstr "Bélyeg_képek megjelenítése"

#: ../settings/xfdesktop-settings-ui.glade.h:50
msgid ""
"Select this option to display preview-able files on the desktop as "
"automatically generated thumbnail icons."
msgstr "Válassza ezt a lehetőséget a fájlok előnézetének megjelenítéséhez az asztalon automatikusan előállított bélyegképekként, ha lehetséges."

#: ../settings/xfdesktop-settings-ui.glade.h:51
msgid "Show hidden files on the desktop"
msgstr "Rejtett fájlok mutatása az asztalon"

#: ../settings/xfdesktop-settings-ui.glade.h:52
msgid "Single _click to activate items"
msgstr "_Elemek aktiválása egy kattintással"

#: ../settings/xfdesktop-settings-ui.glade.h:53
msgid "<b>Appearance</b>"
msgstr "<b>Megjelenés</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:54
msgid "<b>Default Icons</b>"
msgstr "<b>Alapértelmezett ikonok</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:55
msgid "_Icons"
msgstr "I_konok"

#: ../src/menu.c:94
msgid "_Applications"
msgstr "_Alkalmazások"

#: ../src/windowlist.c:73
#, c-format
msgid "Remove Workspace %d"
msgstr "%d. munkaterület eltávolítása"

#: ../src/windowlist.c:74
#, c-format
msgid ""
"Do you really want to remove workspace %d?\n"
"Note: You are currently on workspace %d."
msgstr "Valóban el akarja távolítani a %d. munkaterületet? Megjegyzés: Ön pillanatnyilag a %d. munkaterületen van."

#: ../src/windowlist.c:78
#, c-format
msgid "Remove Workspace '%s'"
msgstr "„%s” munkaterület eltávolítása"

#: ../src/windowlist.c:79
#, c-format
msgid ""
"Do you really want to remove workspace '%s'?\n"
"Note: You are currently on workspace '%s'."
msgstr "Valóban el akarja távolítani a(z) „%s” munkaterületet?\nMegjegyzés: Ön pillanatnyilag a(z) „%s” munkaterületen van."

#. Popup a dialog box confirming that the user wants to remove a
#. * workspace
#: ../src/windowlist.c:86
msgid "Remove"
msgstr "Eltávolítás"

#: ../src/windowlist.c:272
msgid "Window List"
msgstr "Ablaklista"

#: ../src/windowlist.c:297
#, c-format
msgid "<b>Workspace %d</b>"
msgstr "<b>%d. munkaterület</b>"

#: ../src/windowlist.c:393 ../src/windowlist.c:395
msgid "_Add Workspace"
msgstr "M_unkaterület hozzáadása"

#: ../src/windowlist.c:403
#, c-format
msgid "_Remove Workspace %d"
msgstr "%d. munkat_erület eltávolítása"

#: ../src/windowlist.c:406
#, c-format
msgid "_Remove Workspace '%s'"
msgstr "„%s” mu_nkaterület eltávolítása"

#: ../src/xfdesktop-application.c:834
msgid "Display version information"
msgstr "Verzióinformációk megjelenítése"

#: ../src/xfdesktop-application.c:835
msgid "Reload all settings"
msgstr "Minden beállítás újratöltése"

#: ../src/xfdesktop-application.c:836
msgid "Advance to the next wallpaper on the current workspace"
msgstr "Váltás a következő háttérképre a jelenlegi munkaterületen"

#: ../src/xfdesktop-application.c:837
msgid "Pop up the menu (at the current mouse position)"
msgstr "A menü megjelenítése (az aktuális egérpozícióban)"

#: ../src/xfdesktop-application.c:838
msgid "Pop up the window list (at the current mouse position)"
msgstr "Az ablaklista megjelenítése (az aktuális egérpozícióban)"

#: ../src/xfdesktop-application.c:840
msgid "Automatically arrange all the icons on the desktop"
msgstr "Az asztalon lévő ikonok automatikus rendezése"

#: ../src/xfdesktop-application.c:843
msgid "Disable debug messages"
msgstr "Hibakeresési üzenetek letiltása"

#: ../src/xfdesktop-application.c:844
msgid "Do not wait for a window manager on startup"
msgstr "Ne várjon az ablakkezelőre indításkor"

#: ../src/xfdesktop-application.c:845
msgid "Cause xfdesktop to quit"
msgstr "Az xfdesktop bezárása"

#: ../src/xfdesktop-application.c:860
#, c-format
msgid "Failed to parse arguments: %s\n"
msgstr "A paraméterek feldolgozása meghiúsult: %s\n"

#: ../src/xfdesktop-application.c:871
#, c-format
msgid "This is %s version %s, running on Xfce %s.\n"
msgstr "Ez a(z) %s %s verziója, az Xfce %s környezetben.\n"

#: ../src/xfdesktop-application.c:873
#, c-format
msgid "Built with GTK+ %d.%d.%d, linked with GTK+ %d.%d.%d."
msgstr "Összeállítva a GTK+ %d.%d.%d, fut a Gtk+-%d.%d.%d változatával."

#: ../src/xfdesktop-application.c:877
#, c-format
msgid "Build options:\n"
msgstr "Összeállítási beállítások:\n"

#: ../src/xfdesktop-application.c:878
#, c-format
msgid "    Desktop Menu:        %s\n"
msgstr "Asztali menü: %s\n"

#: ../src/xfdesktop-application.c:880 ../src/xfdesktop-application.c:887
#: ../src/xfdesktop-application.c:894
msgid "enabled"
msgstr "engedélyezve"

#: ../src/xfdesktop-application.c:882 ../src/xfdesktop-application.c:889
#: ../src/xfdesktop-application.c:896
msgid "disabled"
msgstr "letiltva"

#: ../src/xfdesktop-application.c:885
#, c-format
msgid "    Desktop Icons:       %s\n"
msgstr "Asztali ikonok: %s\n"

#: ../src/xfdesktop-application.c:892
#, c-format
msgid "    Desktop File Icons:  %s\n"
msgstr "Asztali fájlikonok: %s\n"

#: ../src/xfdesktop-file-icon-manager.c:574
#: ../src/xfdesktop-file-icon-manager.c:592
#, c-format
msgid "Could not create the desktop folder \"%s\""
msgstr "Nem hozható létre az asztal mappa („%s”)"

#: ../src/xfdesktop-file-icon-manager.c:579
#: ../src/xfdesktop-file-icon-manager.c:597
msgid "Desktop Folder Error"
msgstr "Asztal mappa hiba"

#: ../src/xfdesktop-file-icon-manager.c:599
msgid ""
"A normal file with the same name already exists. Please delete or rename it."
msgstr "Már létezik ilyen nevű normál fájl. Törölje vagy nevezze át."

#: ../src/xfdesktop-file-icon-manager.c:683 ../src/xfdesktop-file-utils.c:756
#: ../src/xfdesktop-file-utils.c:820
msgid "Rename Error"
msgstr "Átnevezési hiba"

#: ../src/xfdesktop-file-icon-manager.c:684 ../src/xfdesktop-file-utils.c:821
msgid "The files could not be renamed"
msgstr "A fájlokat nem lehet átnevezni"

#: ../src/xfdesktop-file-icon-manager.c:685
msgid "None of the icons selected support being renamed."
msgstr "A kiválasztott ikonok nem támogatják az átnevezést."

#: ../src/xfdesktop-file-icon-manager.c:989
msgid ""
"This will reorder all desktop items and place them on different screen positions.\n"
"Are you sure?"
msgstr ""

#: ../src/xfdesktop-file-icon-manager.c:993
msgid "_OK"
msgstr "_OK"

#: ../src/xfdesktop-file-icon-manager.c:1053
#, c-format
msgid "_Open With \"%s\""
msgstr "Megnyitás e_zzel: „%s”"

#: ../src/xfdesktop-file-icon-manager.c:1056
#, c-format
msgid "Open With \"%s\""
msgstr "Megnyitás ezzel: „%s”"

#: ../src/xfdesktop-file-icon-manager.c:1129
msgid ""
"Unable to launch \"exo-desktop-item-edit\", which is required to create and "
"edit launchers and links on the desktop."
msgstr "Nem indítható az „exo-desktop-item-edit”, amely szükséges a indítóikonok és hivatkozások létrehozásához az asztalon."

#: ../src/xfdesktop-file-icon-manager.c:1447
msgid "_Open all"
msgstr "Ö_sszes megnyitása"

#: ../src/xfdesktop-file-icon-manager.c:1467
msgid "Create _Launcher..."
msgstr "In_dítóikon létrehozása…"

#: ../src/xfdesktop-file-icon-manager.c:1481
msgid "Create _URL Link..."
msgstr "URL _hivatkozás létrehozása…"

#: ../src/xfdesktop-file-icon-manager.c:1495
msgid "Create _Folder..."
msgstr "_Mappa létrehozása…"

#: ../src/xfdesktop-file-icon-manager.c:1507
msgid "Create _Document"
msgstr "_Dokumentum létrehozása"

#: ../src/xfdesktop-file-icon-manager.c:1532
msgid "No templates installed"
msgstr "Nincsenek telepítve sablonok"

#: ../src/xfdesktop-file-icon-manager.c:1548
msgid "_Empty File"
msgstr "Ü_res fájl"

#: ../src/xfdesktop-file-icon-manager.c:1558
#: ../src/xfdesktop-special-file-icon.c:543 ../src/xfdesktop-volume-icon.c:809
msgid "_Open"
msgstr "_Megnyitás"

#: ../src/xfdesktop-file-icon-manager.c:1574
msgid "_Execute"
msgstr "_Végrehajtás"

#: ../src/xfdesktop-file-icon-manager.c:1592
msgid "_Edit Launcher"
msgstr "Indítóikon s_zerkesztése"

#: ../src/xfdesktop-file-icon-manager.c:1651
msgid "Open With"
msgstr "Megnyitás ezzel"

#: ../src/xfdesktop-file-icon-manager.c:1678
#: ../src/xfdesktop-file-icon-manager.c:1692
msgid "Open With Other _Application..."
msgstr "Megnyitás _más alkalmazással…"

#: ../src/xfdesktop-file-icon-manager.c:1710
msgid "_Paste"
msgstr "_Beilleszt"

#: ../src/xfdesktop-file-icon-manager.c:1728
msgid "Cu_t"
msgstr "_Kivágás"

#: ../src/xfdesktop-file-icon-manager.c:1740
msgid "_Copy"
msgstr "_Másolás"

#: ../src/xfdesktop-file-icon-manager.c:1752
msgid "Paste Into Folder"
msgstr "Beillesztés mappába"

#: ../src/xfdesktop-file-icon-manager.c:1771
msgid "Mo_ve to Trash"
msgstr "Áthelyezés a K_ukába"

#: ../src/xfdesktop-file-icon-manager.c:1784
msgid "_Delete"
msgstr "_Töröl"

#: ../src/xfdesktop-file-icon-manager.c:1802
msgid "_Rename..."
msgstr "Á_tnevezés…"

#: ../src/xfdesktop-file-icon-manager.c:1867
msgid "_Open in New Window"
msgstr "Megnyitás új _ablakban"

#: ../src/xfdesktop-file-icon-manager.c:1876
msgid "Arrange Desktop _Icons"
msgstr "Asztali ikonok _rendezése"

#: ../src/xfdesktop-file-icon-manager.c:1886
msgid "_Next Background"
msgstr "_Következő háttér"

#: ../src/xfdesktop-file-icon-manager.c:1896
msgid "Desktop _Settings..."
msgstr "_Asztal beállításai…"

#: ../src/xfdesktop-file-icon-manager.c:1904
#: ../src/xfdesktop-volume-icon.c:876
msgid "P_roperties..."
msgstr "_Tulajdonságok…"

#: ../src/xfdesktop-file-icon-manager.c:2975
msgid "Load Error"
msgstr "Betöltési hiba"

#: ../src/xfdesktop-file-icon-manager.c:2977
msgid "Failed to load the desktop folder"
msgstr "Az asztal mappa betöltése meghiúsult"

#: ../src/xfdesktop-file-icon-manager.c:3537
msgid "Copy _Here"
msgstr "Másolás i_de"

#: ../src/xfdesktop-file-icon-manager.c:3537
msgid "_Move Here"
msgstr "Á_thelyezés ide"

#: ../src/xfdesktop-file-icon-manager.c:3537
msgid "_Link Here"
msgstr "_Linkelés ide"

#: ../src/xfdesktop-file-icon-manager.c:3572
msgid "_Cancel"
msgstr "_Mégse"

#: ../src/xfdesktop-file-icon-manager.c:3702
msgid "Untitled document"
msgstr "Névtelen dokumentum"

#. TRANSLATORS: file was modified less than one day ago
#: ../src/xfdesktop-file-utils.c:159
#, c-format
msgid "Today at %X"
msgstr "Ma: %X"

#. TRANSLATORS: file was modified less than two days ago
#: ../src/xfdesktop-file-utils.c:163
#, c-format
msgid "Yesterday at %X"
msgstr "Tegnap: %X"

#. Days from last week
#: ../src/xfdesktop-file-utils.c:168
#, c-format
msgid "%A at %X"
msgstr "%A: %X"

#. Any other date
#: ../src/xfdesktop-file-utils.c:171
#, c-format
msgid "%x at %X"
msgstr "%x: %X"

#. the file_time is invalid
#: ../src/xfdesktop-file-utils.c:181
msgid "Unknown"
msgstr "Ismeretlen"

#: ../src/xfdesktop-file-utils.c:683
msgid "The folder could not be opened"
msgstr "A mappa nem nyitható meg"

#: ../src/xfdesktop-file-utils.c:702
msgid "Error"
msgstr "Hiba"

#: ../src/xfdesktop-file-utils.c:703
msgid "The requested operation could not be completed"
msgstr "A kért művelet nem hajtható végre"

#: ../src/xfdesktop-file-utils.c:757
msgid "The file could not be renamed"
msgstr "A fájl nem nevezhető át"

#: ../src/xfdesktop-file-utils.c:758 ../src/xfdesktop-file-utils.c:822
#: ../src/xfdesktop-file-utils.c:883 ../src/xfdesktop-file-utils.c:1047
#: ../src/xfdesktop-file-utils.c:1105 ../src/xfdesktop-file-utils.c:1157
#: ../src/xfdesktop-file-utils.c:1218 ../src/xfdesktop-file-utils.c:1320
#: ../src/xfdesktop-file-utils.c:1382 ../src/xfdesktop-file-utils.c:1457
#: ../src/xfdesktop-file-utils.c:1537
msgid ""
"This feature requires a file manager service to be present (such as the one "
"supplied by Thunar)."
msgstr "Ez a szolgáltatás egy fájlkezelő-szolgáltatás jelenlétét igényli (például a Thunar által biztosítottat)."

#: ../src/xfdesktop-file-utils.c:881
msgid "Delete Error"
msgstr "Törlési hiba"

#: ../src/xfdesktop-file-utils.c:882
msgid "The selected files could not be deleted"
msgstr "A kiválasztott fájlok nem törölhetők"

#: ../src/xfdesktop-file-utils.c:942 ../src/xfdesktop-file-utils.c:990
msgid "Trash Error"
msgstr "Kuka hiba"

#: ../src/xfdesktop-file-utils.c:943
msgid "The selected files could not be moved to the trash"
msgstr "A kiválasztott fájlok nem helyezhetők át a Kukába"

#: ../src/xfdesktop-file-utils.c:944 ../src/xfdesktop-file-utils.c:992
msgid ""
"This feature requires a trash service to be present (such as the one "
"supplied by Thunar)."
msgstr "Ez a szolgáltatás egy kukaszolgáltatás jelenlétét igényli (például a Thunar által biztosítottat)."

#: ../src/xfdesktop-file-utils.c:991
msgid "Could not empty the trash"
msgstr "A Kuka nem üríthető"

#: ../src/xfdesktop-file-utils.c:1045
msgid "Create File Error"
msgstr "Fájllétrehozási hiba"

#: ../src/xfdesktop-file-utils.c:1046
msgid "Could not create a new file"
msgstr "Nem hozható létre új fájl"

#: ../src/xfdesktop-file-utils.c:1103
msgid "Create Document Error"
msgstr "Dokumentum-létrehozási hiba"

#: ../src/xfdesktop-file-utils.c:1104
msgid "Could not create a new document from the template"
msgstr "Nem hozható létre dokumentum a sablonból"

#: ../src/xfdesktop-file-utils.c:1155
msgid "File Properties Error"
msgstr "Fájltulajdonságok-hiba"

#: ../src/xfdesktop-file-utils.c:1156
msgid "The file properties dialog could not be opened"
msgstr "A fájl tulajdonságai ablak nem nyitható meg"

#: ../src/xfdesktop-file-utils.c:1217
msgid "The file could not be opened"
msgstr "A fájl nem nyitható meg"

#: ../src/xfdesktop-file-utils.c:1291 ../src/xfdesktop-file-utils.c:1315
#, c-format
msgid "Failed to run \"%s\""
msgstr "„%s” futtatása meghiúsult"

#: ../src/xfdesktop-file-utils.c:1381
msgid "The application chooser could not be opened"
msgstr "Az alkalmazásválasztó nem nyitható meg"

#: ../src/xfdesktop-file-utils.c:1440 ../src/xfdesktop-file-utils.c:1455
#: ../src/xfdesktop-file-utils.c:1518 ../src/xfdesktop-file-utils.c:1535
msgid "Transfer Error"
msgstr "Átviteli hiba"

#: ../src/xfdesktop-file-utils.c:1441 ../src/xfdesktop-file-utils.c:1456
#: ../src/xfdesktop-file-utils.c:1519 ../src/xfdesktop-file-utils.c:1536
msgid "The file transfer could not be performed"
msgstr "A fájlátvitel nem hajtható végre"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:127
msgid "Unmounting device"
msgstr "Eszköz leválasztása"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:130
#, c-format
msgid ""
"The device \"%s\" is being unmounted by the system. Please do not remove the"
" media or disconnect the drive"
msgstr "A(z) „%s” eszköz leválasztása a rendszerről. Ne távolítsa el az adathordozót, és ne kapcsolja le a meghajtót"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:137 ../src/xfdesktop-notify.c:322
msgid "Writing data to device"
msgstr "Adatok eszközre írása"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:140 ../src/xfdesktop-notify.c:325
#, c-format
msgid ""
"There is data that needs to be written to the device \"%s\" before it can be"
" removed. Please do not remove the media or disconnect the drive"
msgstr "Adatokat kell a(z) „%s” eszközre írni annak eltávolítása előtt. Ne távolítsa el az adathordozót, és ne kapcsolja le a meghajtót"

#: ../src/xfdesktop-notify.c:221
msgid "Unmount Finished"
msgstr "Leválasztva"

#: ../src/xfdesktop-notify.c:223 ../src/xfdesktop-notify.c:408
#, c-format
msgid "The device \"%s\" has been safely removed from the system. "
msgstr "A(z) „%s” eszköz biztonságosan el lett távolítva a rendszerből."

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:313
msgid "Ejecting device"
msgstr "Eszköz kiadása"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:316
#, c-format
msgid "The device \"%s\" is being ejected. This may take some time"
msgstr "A(z) „%s” eszköz kiadása. Ez eltarthat egy ideig"

#: ../src/xfdesktop-notify.c:406
msgid "Eject Finished"
msgstr "Kiadva"

#: ../src/xfdesktop-regular-file-icon.c:820
#, c-format
msgid ""
"Name: %s\n"
"Type: %s\n"
"Size: %s\n"
"Last modified: %s"
msgstr "Név: %s\nTípus: %s\nMéret: %s\nUtolsó módosítás: %s"

#: ../src/xfdesktop-special-file-icon.c:459
msgid "Trash is empty"
msgstr "A Kuka üres"

#: ../src/xfdesktop-special-file-icon.c:462
msgid "Trash contains one item"
msgstr "A Kuka egy elemet tartalmaz"

#: ../src/xfdesktop-special-file-icon.c:463
#, c-format
msgid "Trash contains %d items"
msgstr "A Kuka %d elemet tartalmaz"

#: ../src/xfdesktop-special-file-icon.c:492
#, c-format
msgid ""
"%s\n"
"Size: %s\n"
"Last modified: %s"
msgstr "%s\nMéret: %s\nUtolsó módosítás: %s"

#: ../src/xfdesktop-special-file-icon.c:559
msgid "_Empty Trash"
msgstr "_Kuka ürítése"

#: ../src/xfdesktop-volume-icon.c:478
#, c-format
msgid ""
"Removable Volume\n"
"Mounted in \"%s\"\n"
"%s left (%s total)"
msgstr "Cserélhető kötet\nCsatolási pont: %s\n%s elérhető (összesen %s)"

#: ../src/xfdesktop-volume-icon.c:485
msgid ""
"Removable Volume\n"
"Not mounted yet"
msgstr "Cserélhető kötet\nMég nincs csatolva"

#: ../src/xfdesktop-volume-icon.c:514 ../src/xfdesktop-volume-icon.c:560
#, c-format
msgid "Failed to eject \"%s\""
msgstr "„%s” kiadása meghiúsult"

#: ../src/xfdesktop-volume-icon.c:519 ../src/xfdesktop-volume-icon.c:565
msgid "Eject Failed"
msgstr "A kiadás meghiúsult"

#: ../src/xfdesktop-volume-icon.c:598
#, c-format
msgid "Failed to mount \"%s\""
msgstr "„%s” csatolása meghiúsult"

#: ../src/xfdesktop-volume-icon.c:601
msgid "Mount Failed"
msgstr "A csatolás meghiúsult"

#: ../src/xfdesktop-volume-icon.c:835
msgid "_Safely Remove Volume"
msgstr ""

#: ../src/xfdesktop-volume-icon.c:838
msgid "_Disconnect Volume"
msgstr ""

#: ../src/xfdesktop-volume-icon.c:841
msgid "_Stop the Multi-Disk Drive"
msgstr ""

#: ../src/xfdesktop-volume-icon.c:844
msgid "_Lock Volume"
msgstr ""

#: ../src/xfdesktop-volume-icon.c:847
msgid "E_ject Volume"
msgstr "Kötet ki_adása"

#: ../src/xfdesktop-volume-icon.c:856
msgid "_Unmount Volume"
msgstr "Kötet _leválasztása"

#: ../src/xfdesktop-volume-icon.c:863
msgid "_Mount Volume"
msgstr "Kötet _csatolása"

#: ../src/xfdesktop-window-icon.c:193
msgid "_Window Actions"
msgstr "_Ablakműveletek"
